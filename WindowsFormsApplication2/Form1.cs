﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;
namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        FilterInfoCollection webcam;
        VideoCaptureDevice cam;
        int renk = 0;
        private void Form1_Load(object sender, EventArgs e)
        {
            webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo c in webcam)
            {
                toolStripComboBox1.Items.Add(c.Name);
            }
            toolStripComboBox1.SelectedIndex = 0;

            cam = new VideoCaptureDevice(webcam[toolStripComboBox1.SelectedIndex].MonikerString);
            cam.NewFrame += cam_NewFrame; ;
            cam.Start();



        }

        void cam_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (renk == 0)
            {
                pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
            }
            if ( renk == 1)
            {
            Bitmap SourceImg = (Bitmap)eventArgs.Frame.Clone();
            Bitmap outputimg = new Bitmap(SourceImg.Width, SourceImg.Height);

            BitmapData bmd1, bmd2;
            Rectangle Rect = new Rectangle(0, 0, SourceImg.Width, SourceImg.Height);
            bmd1 = SourceImg.LockBits(Rect, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            bmd2 = outputimg.LockBits(Rect, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                byte* ptr1 = (byte*)bmd1.Scan0;
                byte* ptr2 = (byte*)bmd2.Scan0;
                for (int i = 0; i < bmd1.Height * bmd1.Stride; i+=3)
                {
                    int B = ptr1[i], G = ptr1[i + 1], R = ptr1[i + 2];
                  
                    int binary;
                    if (B > 0 && B < 255 && R> 0 && R < 40 && G > 0 && G < 40  )
                    {
                        ptr2[i] = (byte)B;
                        ptr2[i + 1] = (byte)G;
                        ptr2[i + 2] = (byte)R;
                    }
                    else
                    {
                        binary = 0;
                        ptr2[i] = (byte)binary;
                        ptr2[i + 1] = (byte)binary;
                        ptr2[i + 2] = (byte)binary;
                    }
                }
                SourceImg.UnlockBits(bmd1);
                outputimg.UnlockBits(bmd2);
            }
            pictureBox1.Image = outputimg;
        }
            if (renk == 2)
            {
                Bitmap SourceImg = (Bitmap)eventArgs.Frame.Clone();
                Bitmap outputimg = new Bitmap(SourceImg.Width, SourceImg.Height);

                BitmapData bmd1, bmd2;
                Rectangle Rect = new Rectangle(0, 0, SourceImg.Width, SourceImg.Height);
                bmd1 = SourceImg.LockBits(Rect, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                bmd2 = outputimg.LockBits(Rect, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                unsafe
                {
                    byte* ptr1 = (byte*)bmd1.Scan0;
                    byte* ptr2 = (byte*)bmd2.Scan0;
                    for (int i = 0; i < bmd1.Height * bmd1.Stride; i += 3)
                    {
                        int B = ptr1[i], G = ptr1[i + 1], R = ptr1[i + 2];
                        int binary;
                        if (B > 0 && B < 40 && R > 0 && R < 255 && G > 0 && G < 40)
                        {
                            ptr2[i] = (byte)B;
                            ptr2[i + 1] = (byte)G;
                            ptr2[i + 2] = (byte)R;
                        }
                        else
                        {
                            binary = 0;
                            ptr2[i] = (byte)binary;
                            ptr2[i + 1] = (byte)binary;
                            ptr2[i + 2] = (byte)binary;
                        }
                    }
                    SourceImg.UnlockBits(bmd1);
                    outputimg.UnlockBits(bmd2);
                }
                pictureBox1.Image = outputimg;
            }
            if (renk == 3)
            {
                Bitmap SourceImg = (Bitmap)eventArgs.Frame.Clone();
                Bitmap outputimg = new Bitmap(SourceImg.Width, SourceImg.Height);

                BitmapData bmd1, bmd2;
                Rectangle Rect = new Rectangle(0, 0, SourceImg.Width, SourceImg.Height);
                bmd1 = SourceImg.LockBits(Rect, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                bmd2 = outputimg.LockBits(Rect, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                unsafe
                {
                    byte* ptr1 = (byte*)bmd1.Scan0;
                    byte* ptr2 = (byte*)bmd2.Scan0;
                    for (int i = 0; i < bmd1.Height * bmd1.Stride; i += 3)
                    {
                        int B = ptr1[i], G = ptr1[i + 1], R = ptr1[i + 2];
                        int binary;
                        if (B > 0 && B < 80 && R > 0 && R < 80 && G > 0 && G < 255)
                        {
                            ptr2[i] = (byte)B;
                            ptr2[i + 1] = (byte)G;
                            ptr2[i + 2] = (byte)R;
                        }
                        else
                        {
                            binary = 0;
                            ptr2[i] = (byte)binary;
                            ptr2[i + 1] = (byte)binary;
                            ptr2[i + 2] = (byte)binary;
                        }
                    }
                    SourceImg.UnlockBits(bmd1);
                    outputimg.UnlockBits(bmd2);
                }
                pictureBox1.Image = outputimg;
            }
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cam.Stop();
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renk = 1;

               }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renk = 2;
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renk = 3;
        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {
            renk = 0;
            cam.Start();

        }

        private void cIKISToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.Save("f:\\test.jpg");
            cam.Start();
        }
    }
}
